* Be careful for `rm -rf`, if you have to do it, leave 1 second to consider it.
* golang's go-func in for-range.

```
func main() {
	 for i := range []int{8,9,10} {
	   go func() {
		 fmt.Printf("int=%d\n", i)
	   }()
	 }
	  time.Sleep(time.Second)
}
```
The codes will nearly print three '2'.

For correcting it:

```
go func(myi int) {...}(i)
```